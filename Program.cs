﻿using System;
namespace Advent {
  class Advent {
      
      static void calculate_depth_forward(string fname){
	  using (var reader = new StreamReader(fname)){
	      string line;
	      int hor_depth = 0;
	      int ver_depth = 0;
	      while((line = reader.ReadLine()) != null){
		  string[] split = line.Split(' ');
		  string dir = split[0];
		  int incr = int.Parse(split[1]);
		  switch (dir) {
		      case "forward":
			  hor_depth += incr;
			  break;
		      case "up":
			  ver_depth -= incr;
			  break;
		      case "down":
			  ver_depth += incr;
			  break;
		      default:
			  Console.WriteLine($"I don't understand {dir}");
			  break;
		  }
		  Console.WriteLine($"hor_depth {hor_depth} ver_depth {ver_depth}");
	      }
	  }
	  
      }
      // Day 1 answer
      static void calculate_depth(string fname){
	  int[] lines = Array.ConvertAll(File.ReadAllLines(fname),int.Parse);
	  int depth = 0;
	  int increase = 0;
	  int window_size = 3;
	  int i = 0;
	  while (i != lines.Length){
	      if (i == 0){
		  for (int ind = 0; ind < window_size; ind++) {
		      depth += lines[ind];
		  }
		      i += window_size;
	      }
	      else{
		  int new_depth = (depth - lines[i - window_size]) + lines[i];
		  if (new_depth > depth) {
		      increase++; 
					 }
		  depth = new_depth;
		  i++;
	  }
	      
      }
	  Console.WriteLine($"Increase: {increase}");
      }
      
    public static void Main(string[] args) {
	//	calculate_depth("depth.txt");
	calculate_depth_forward("movement.txt");
   }
  }
}
